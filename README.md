# Front-End Projects

## Description
This is a repository that will contain multiple different commits, which will invlove different front-end projects. These projects will include HTML5, CSS3, CSS frameworks such as Bootstrap and Bulma, JavaScript, and JavaScript frameworks like React, JQuery,

## Installation
Most of the projects will be pre-installed with the required files, so hopefully there won't be a need to install any packages or libraries.

## Usage
These are just simple front-end projects that I have made in my own time. Some these are design focused, while others are more technical.

## Roadmap
Most of these projects are simplistic, but I may upload more advanced projects down the line.
##### 1. Text Editor
This was a simple JS text editor that I had built to integrate into my blogging site, but due to the amount of XSS vulnerabilities it could cause I did not integrate it into my site.

##### 2. TESLA 
This was just a simple responsive front-end clone a single Tesla.com page. 

##### 3. simply-responsive
This was a responsive grid layout that I had built to experiment with Bulma.

##### 4. Theme-Changer
This was a simple project that I had created to explore how dark-mode, light-mode, and other theme changes could be implemented into a website.

## Contributing
Since these are my personal projects, there have been no other contributors to them.

## Authors and acknowledgment
Syed Aby

## License
These projects are personal and taking these without persmission would be very disingenuos of whoever does so.

## Project status
Will get updated as I complete more of my incomplete projects.
