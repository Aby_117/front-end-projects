const darkButton = document.getElementById('dark');
const lightButton = document.getElementById('light');
const solarButton = document.getElementById('solar');
const body = document.body;

//Apply the cache theme on reload

const theme = localStorage.getItem('theme');
const isSolar = localStorage.getItem('isSolar');

if (theme){
	body.classList.add(theme);
	isSolar && body.classList.add('solarize');
}

//Button Event Handlers
//Same thing as the code above

darkButton.onclick = function() {
	body.classList.replace('light', 'dark');
	localStorage.setItem('theme', 'dark');
}

lightButton.onclick = () => {
	//.replace is easier to use than .add and .remove 
	body.classList.replace('dark', 'light');
	localStorage.setItem('theme', 'light');
};

solarButton.onclick = () => {
	if (body.classList.contains('solarize')) {
		body.classList.remove('solarize');
		/*Edits the CSS file*/
		solarButton.style.cssText = `--bg-solar: var(--yellow);`

		solarButton.innerText = 'solarize';	

		localStorage.removeItem('isSolar');

	} else {
		solarButton.style.cssText = `--bg-solar: lightblue;`

		body.classList.add('solarize');

		solarButton.innerText = 'normalize';

		localStorage.setItem('isSolar', true);
	} 
};

